# README #

### Configuration of NodeJS ###
Installation
Download and install NodeJS https://nodejs.org/en/

* Version "Recommended for Most Users"
 
Proceed to typical install, in C:\Program Files\nodejs

### Create the application folder ###

Create folder(s) to have a tree like this : 	 C:\nodeApps\qrstools
In the command prompt (cmd), go to this directory and execute :

* C:\nodeApps\qrstools\npm init 

It will initialize the project and create the packages.json file.
Then we need Additional Packages, run in the command line :

* C:\nodeApps\qrstools\npm install express-generator
* C:\nodeApps\qrstools\Npm install express –-save
* C:\nodeApps\qrstools\Npm install node-fetch –-save 
* C:\nodeApps\qrstools\Npm install node-windows --save
* C:\nodeApps\qrstools\Npm install fs --save
* C:\nodeApps\qrstools\Npm install https --save

### Generate Self-signed SSL certificates

Download OpenSSL (version  Win64 OpenSSL v1.1.1g)
* https://slproweb.com/products/Win32OpenSSL.html

Install on Windows, C:\Program Files\\OpenSSL-Win64

After install go to  C:\nodeApps\qrstools

* Execute : "C:\Program Files\OpenSSL-Win64\bin\openssl"

Then the openssl bash command line appear, enter  :

* openssl>req -nodes -new -x509 -keyout server.key -out server.cert
