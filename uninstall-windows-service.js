var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'QRS Tools - NodeJS Application',
  description: 'QRS Tools as Windows Service',
  script: 'C:\\nodeApps\\qrstools\\index.js'
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

// Uninstall the service.
svc.uninstall();