var fetch = require('node-fetch');
var express = require("express");
var fs = require('fs');
var https = require('https');

var app = express();

// Welcome page -- to be set at the root endpoint in the Qlik Rest connector
app.get("/about", (req, res, next) => {
  res.json(["Welcome! You are well connected to the QRS API tool module."]);
});


// to copy an Qlik application + rename if a new name is provided.
app.get("/makecopy", (req, res, next) => {

  const appid=req.query.app;
  const copyname=req.query.name;

  console.log ('Make Copy - Request Parameters : app=' + appid + '| name=' + copyname )

  const baseUrl = 'https://sensedev.agilos.com/apis/qrs/app/';

  const xrf = '123456789ABCDEFG';

  const options = {
    method: 'POST',
    body: '',
    headers: {
          'X-Qlik-xrfkey' : '123456789ABCDEFG',
          'api_usr' : 'mla'
    }
  };

  const qrsUrl = baseUrl + appid + '/copy?Xrfkey=' + xrf + '&name=' + copyname;

  fetch(qrsUrl, options)
      .then(response => response.json())
      .then(function (data) {
          console.log('Request succeeded with JSON response', data); 
          res.send(data);
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });

});





// to publish an app in a stream + rename if a new name is provided.
app.get("/publish", (req, res, next) => {

  const appid=req.query.app;
  const streamid=req.query.stream;
  const publishname=req.query.name;

  console.log (' Publish - Request Parameters : app=' + appid + ' | stream =' + streamid +' | name=' + publishname )

  const baseUrl = 'https://sensedev.agilos.com/apis/qrs/app/';

  const xrf = '123456789ABCDEFG';

  const options = {
  	method: 'PUT',
  	body: '',
  	headers: {
      		'X-Qlik-xrfkey' : '123456789ABCDEFG',
      		'api_usr' : 'mla'
  	}
  };

  const qrsUrl = baseUrl + appid + '/publish?Xrfkey=' + xrf + '&stream=' + streamid + '&name=' + publishname;
  //const qrsUrl = "https://sensedev.agilos.com/apis/qrs/app/{12fb5c3f-b0f8-4fb7-890c-1d0342bec62e}/publish?Xrfkey=123456789ABCDEFG&stream=8c4e44b2-d992-41d1-8e2a-7dacd79c0dc4&name=PublishedFromAPI";

  fetch(qrsUrl, options)
      .then(response => response.json())
      .then(function (data) {
          console.log('Request succeeded with JSON response', data); 

          // handle response to conform json answer for Qlik and add request status before sendin response to Qlik
          var qResponse = data;
          if  ( Array.isArray(qResponse) ) {
              qResponse[0].requestStatus = "ERROR - " + qResponse[0].errorText;
          } else {
              qResponse.requestStatus = "SUCCESS - Application has been published."
          }
          res.send(qResponse)
      })
      .catch(function (error) {
        console.log('Request failed', error);
      });
});

// Listener
https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app)
.listen(3033, function () {
  console.log('Server HTTPS running on port 3033.')
})