var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'QRS Tools - NodeJS Application',
  description: 'QRS Tools as Windows Service',
  script: 'C:\\nodeApps\\qrstools\\index.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();